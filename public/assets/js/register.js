let registerForm = document.querySelector("#registerUser")

registerForm.addEventListener("submit", (e) => {
	
	e.preventDefault(); 
	
	let fName = document.querySelector("#firstName").value;
	let lName = document.querySelector("#lastName").value;
	let uEmail = document.querySelector("#userEmail").value;
	let mNumber = document.querySelector("#mobileNumber").value;
	let password = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	/*message*/
	let errorMessage = document.querySelector("#error-message");
	let successMessage = document.querySelector("#success-message");

	//information validation upon creating a new entry in the database.
	//lets create a control structure
	//=> to check if passwords match
	//=> to check if passwords are not empty
	//=> to check the validation of mobile number, what we can do is to check the length of the mobile number input
	if((password !== "" && password2 !== "") && (password2 === password) && (mNumber.length === 11)) {
		fetch('https://radiant-tundra-86932.herokuapp.com/api/users/email-exist', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: uEmail
			})
		}).then(res => res.json()
		)//this will give the information if there are no duplicates found
		.then(data => {
			if(data === false) {
				fetch("https://radiant-tundra-86932.herokuapp.com/api/users/register", {
							//here we built the structure of our request
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							//once you have identified the properties of the docuement, 
						    firstName: fName,
							lastName: lName,
							email: uEmail,
							mobileNo: mNumber,
							password: password
						})
							//next is to create a promise to identify what will happen if the response from the backend (in the controller) is positive or negative
					}).then(res => {
						return res.json()
						//the response i got is either true or false
						//then lets create another promise that will display the response from the backend to our front end.
					}).then(data => {
						console.log(data) //check if we were able to get the data from our backend project.
						//create a control structure that will determine the response in my front end app.
						if(data === true) {
							//alert("New user added successfully.")
							return successMessage.innerHTML = "New user added successfully."
						} else {
							alert("Something went wrong in the registration.")
							//return errorMessage.innerHTML = "Something went wrong in the registration."
						}
					})
			}else {
				alert(`Email is already exists. Choose another email.`)
				//return errorMessage.innerHTML = "Email already exists. Choose another email."
			}
		})
	}else {
		alert("Something went wrong please check your credentials. All fields are required.")
		//return errorMessage.innerHTML = "Something went wrong please check your credentials. All fields are required."
	}
})