let navItems =  document.querySelector("#navSession");
//console.log(navItems)
let profile = document.querySelector("#profile");
let register = document.querySelector("#register");
//capture the navSession element inside the navbar component
let userToken = localStorage.getItem("token")
//lets take the access token from the local storage

console.log(userToken)
//lets create a control structure that will determine which elements inside the nav bar will be displayed if a user token is found in the local storage.
 if(!userToken) {
	navItems.innerHTML = 
				`
					<li class="nav-item">
						<a href="./pages/login.html" class="nav-link"> Log in </a>
					</li>
				`
} else {
	navItems.innerHTML = `
				<li class="nav-item">
					<a href="./pages/logout.html" class="nav-link"> Log Out </a>
				</li>
	`
	profile.innerHTML = `
				<li class="nav-item">
					<a href="./pages/profile.html" class="nav-link"> Profile </a>
				</li>
	`

	register.remove();
}