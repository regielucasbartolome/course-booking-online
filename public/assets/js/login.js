//console.log('login')
let loginForm = document.querySelector('#loginUser')

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector('#userEmail').value;
	//console.log(email)
	let password = document.querySelector('#password').value;
	let message = document.querySelector('.message');
	//console.log(password)
	//how can we inform a user that a blank input field cannot be logged in?

	if(email == "" || password == "") {
		//alert('Please input your email and password.')
		return message.innerHTML = "Please input your correct email and password."
				
	}else {
		fetch('https://radiant-tundra-86932.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
					email: email,
					password: password
				})
		}).then(res => {
			return res.json()
		}).then(data => {
			console.log(data.access)
			if(data.access){
				//lets save the access token inside our local storage.
				localStorage.setItem('token', data.access)//this local storage can be found inside the subfolder of the appData of the local file of the google chrome browser inside the data module of the user folder.
				//alert('Access key saved on local storage.') //for educational purposes only
				fetch(`https://radiant-tundra-86932.herokuapp.com/api/users/details`, {
					headers: {
						'Authorization': `Bearer ${data.access}`
					}
				}).then(res => {
					return res.json()
				}).then(data => {
					console.log(data);
					localStorage.setItem("id", data._id) //this came from the payload.
					localStorage.setItem("isAdmin", data.isAdmin)
					//console.log("items are set inside the local storage.")
					//direct the user to the courses page upon successful login.
					window.location.replace('./courses.html')
				})
			}else {
				//if there is no existing access key value then just inform the user.
				alert('Something went wrong. Check your credentials.')
			}
		})
	}
})