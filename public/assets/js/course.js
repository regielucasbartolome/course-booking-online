console.log('Hello')

//The first thing that we need to do is to identify whih course it needs to display inside the browser.
//we are going to use the course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search)
//windows.location -> returns a location object with information about the "current" location of the document
//.search => contains the query string section of the current URL.
//search property returns an object of type stringString
//URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object. (this is a class)
//"URLSearchParams" -> describes the interface that defines utility methods to work with the query string of a URL (this is a prop type/template)
//new -> instantiates a user-defined object.

/*params = {
	"courseId": "id ng course that we passed"
}*/
let id = params.get('courseId');

console.log(id)

//lets capture the sections of the html body
let name  = document.querySelector("#courseName");
let desc  = document.querySelector("#courseDesc");
let price  = document.querySelector("#coursePrice");

fetch(`https://radiant-tundra-86932.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
	console.log(data)
	name.innerHTML = data.name
	desc.innerHTML = data.description
	price.innerHTML = data.price
}) //if the result is displayed in the console. It means you were able to properly pass teh courseId and successfully created your fetch request.